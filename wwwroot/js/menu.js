﻿$(document).ready(function () {
    $(".dropdown").mousemove(function () {
        $(this).find(".dropdown-toggle").trigger("click").unbind("mousemove");
        $(this).unbind("mousemove");
    });

    $(".dropdown").mouseout(function () {
        $(this).find(".dropdown-toggle").trigger("click").unbind("mousemove");
        $(this).mousemove(function () {
            $(this).find(".dropdown-toggle").trigger("click").unbind("mousemove");
            $(this).unbind("mousemove");
        });
    });
})